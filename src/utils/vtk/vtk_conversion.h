//
// Created by duboisj on 03/07/2020.
//

#ifndef OPENHTG_VTK_CONVERSION_H
#define OPENHTG_VTK_CONVERSION_H
class openHyperTreeGrid;
class vtkHyperTreeGrid;
class vtkUniformHyperTreeGrid;

namespace openhtg {
namespace utils {
namespace conversion {

void convert_openhtg_to_vtkhtg( openHyperTreeGrid & ohtg, vtkUniformHyperTreeGrid* uhtg );
void convert_vtkhtg_to_openhtg( vtkHyperTreeGrid* htg, openHyperTreeGrid & ohtg );

} //conversion
} //utils
} //openhtg


#endif // OPENHTG_VTK_CONVERSION_H
