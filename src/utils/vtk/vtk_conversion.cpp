//
// Created by duboisj on 03/07/2020.
//

#include "vtk_conversion.h"

#include "openHyperTree.h"
#include "openHyperTreeGrid.h"
#include "openHyperTreeGridCursor.h"

#include "vtkBitArray.h"
#include "vtkCellData.h"
#include "vtkFloatArray.h"
#include "vtkHyperTreeGridNonOrientedCursor.h"
#include "vtkSmartPointer.h"
#include "vtkUniformHyperTreeGrid.h"
#include <map>
#include <vector>

#include <iostream>

void initVTKUniformHTGFromOpenHTG(vtkUniformHyperTreeGrid * uhtg, openHyperTreeGrid & ohtg)
{
  std::array<uint32_t, 3> dims{};
  ohtg.getDimensions(dims.data());
  std::cout << "OHTG dims: " << dims[0] << "," << dims[1] << "," << dims[2] << "\n";
  uhtg->SetDimensions(dims[0]+1, dims[1]+1, dims[2]+1);
  uhtg->SetBranchFactor(2);
  const double * scale = ohtg.getScale(0);
  std::cout << "OHTG scale: " << scale[0] << "," << scale[1] << "," << scale[2] << "\n";
  uhtg->SetGridScale(scale[0]);
  std::array<double, 6> bounds{};
  ohtg.getBounds(bounds.data());
  std::cout << "OHTG bounds: " << bounds[0] << "," << bounds[1] << "," << bounds[2] << "," << bounds[3] << "," << bounds[4] << "," << bounds[5] <<"\n";
  std::cout << "OHTG origin: " << bounds[0] << "," << bounds[2] << "," << bounds[4] <<"\n";
  uhtg->SetOrigin(bounds[0], bounds[2], bounds[4]);

  vtkNew<vtkBitArray> mask;
  uhtg->SetMask(mask);
}
void initOpenHTGFromVTKHyperTreeGrid(vtkHyperTreeGrid *htg, openHyperTreeGrid & ohtg)
{
  std::array<uint32_t, 3> dims{};
  htg->GetDimensions(dims.data());
  ohtg.setDimensions(dims[0]-1, dims[1]-1, dims[2]-1);
  std::cout << "OHTG dims: " << dims[0]-1 << "," << dims[1]-1 << "," << dims[2]-1 << "\n";
  const double * bounds = htg->GetBounds();
  const std::array<double, 3> minBounds = {bounds[0], bounds[2], bounds[4]};
  const std::array<double, 3> maxBounds = {bounds[1], bounds[3], bounds[5]};
  ohtg.setBoundingBox( minBounds.data(), maxBounds.data());
  std::cout << "OHTG bounds: " << bounds[0] << "," << bounds[1] << "," << bounds[2] << "," << bounds[3] << "," << bounds[4] << "," << bounds[5] <<"\n";
  std::cout << "OHTG origin: " << bounds[0] << "," << bounds[2] << "," << bounds[4] <<"\n";

}


typedef std::map<std::string, std::pair< std::vector<float>*, vtkFloatArray* > > float_attributes_t;

struct FloatAttributeManager {
  virtual void copy_attributes(uint32_t ohtg_idx, vtkIdType uhtg_idx) = 0;
};

template <class Cursor_t>
class OHTGCursorAdaptor {
public:
  OHTGCursorAdaptor(Cursor_t & cursor):_cursor(cursor){}
  void ToChild(uint32_t idx){this->_cursor.toChild(idx);}
  void ToParent(){this->_cursor.toParent();}
  [[nodiscard]] uint32_t GetGlobalNodeIndex() const{ return this->_cursor.getGlobalIndex();}
  [[nodiscard]] bool IsMasked() const { return this->_cursor.isMasked();}
  [[nodiscard]] uint32_t GetTreeIndex() const{ return this->_cursor.getTreeIndex();}
  bool ToNextTree(){ return this->_cursor.toNextTree();}
  [[nodiscard]] bool IsLeaf() const {return this->_cursor.isLeaf();}
  void SetMask(bool b){this->_cursor.setMask(b);}
  void SubdivideLeaf(){this->_cursor.subdivideLeaf();}
  [[nodiscard]] unsigned int GetNumberOfChildren() const {return this->_cursor.getNumberOfChildren();}
  Cursor_t getCursor() {return this->_cursor;}
private:
  Cursor_t &_cursor;
};

template <class CursorIn_t, class CursorOut_t>
void dfsCopyTree( CursorIn_t &cursorIn, CursorOut_t&cursorOut, FloatAttributeManager & attributes)
{
  const bool isMasked = cursorIn.IsMasked();
  const uint32_t inhtg_idx = cursorIn.GetGlobalNodeIndex();
  const vtkIdType outhtg_idx = cursorOut.GetGlobalNodeIndex();
  // copy attributes
  attributes.copy_attributes(inhtg_idx, outhtg_idx);
  // mask if needed
  cursorOut.SetMask( isMasked );

  // subdivide if needed
  if ( !cursorIn.IsLeaf() )
  {
    cursorOut.SubdivideLeaf();
    for (unsigned char i = 0 ; i < cursorOut.GetNumberOfChildren() ; ++i)
    {
      cursorIn.ToChild(i);
      cursorOut.ToChild(i);
      dfsCopyTree(cursorIn, cursorOut, attributes);
      cursorOut.ToParent();
      cursorIn.ToParent();
    }
  }
}

struct FloatAttributeOHTGToUHTG: public FloatAttributeManager{
  typedef std::map<std::string, std::pair< std::vector<float>*, vtkFloatArray* > > float_attributes_ohtg_to_uhtg_t;

  FloatAttributeOHTGToUHTG(openHyperTreeGrid & ohtg, vtkUniformHyperTreeGrid* uhtg) {
    auto arrayNames = ohtg.getscalarArrayNames();
    for (const auto &array_name : arrayNames) {
      auto *ohtg_array = &(ohtg.getscalarArray(array_name));
      vtkNew<vtkFloatArray> vtk_attribute;
      vtk_attribute->SetName(array_name.c_str());
      vtk_attribute->SetNumberOfValues(ohtg_array->size());
      uhtg->GetCellData()->AddArray(vtk_attribute);

      this->float_attributes.emplace(std::make_pair(
          array_name, std::make_pair(ohtg_array, vtk_attribute.Get())));
    }
  }
  void copy_attributes(uint32_t ohtg_idx, vtkIdType uhtg_idx) override
  {
    for ( const auto& elements: this->float_attributes)
    {
      const auto * ohtg_att = elements.second.first;
      const auto & uhtg_att = elements.second.second;
      uhtg_att->SetValue( uhtg_idx, (*ohtg_att)[ohtg_idx] );
    }
  }

  float_attributes_ohtg_to_uhtg_t float_attributes;
};

struct FloatAttributeHTGToOHTG: public FloatAttributeManager{
  typedef std::map<std::string, std::pair< vtkDataArray*, std::vector<float>* > > float_attributes_htg_to_ohtg_t;

  FloatAttributeHTGToOHTG(vtkHyperTreeGrid* htg, openHyperTreeGrid & ohtg ) {
    const int nbArrays = htg->GetCellData()->GetNumberOfArrays();
    for (int i = 0 ; i < nbArrays; ++i) {
      auto * array = htg->GetCellData()->GetArray(i);
      const bool ok =  (array->IsA("vtkFloatArray") || array->IsA("vtkDoubleArray") || array->IsA("vtkIntArray") || array->IsA("vtkIdTypeArray"));
      if (!ok)
      {
        continue;
      }
      const std::string array_name {array->GetName()};
      ohtg.addScalarField(array_name);
      auto &ohtg_array = ohtg.getscalarArray(array_name);
      ohtg_array.resize(array->GetNumberOfValues());

      this->float_attributes.emplace(std::make_pair(
          array_name, std::make_pair(array, (&ohtg_array))));
    }
  }
  void copy_attributes(uint32_t htg_idx, vtkIdType ohtg_idx) override
  {
    for ( const auto& elements: this->float_attributes)
    {
      auto * htg_att = elements.second.first;
      const auto & ohtg_att = elements.second.second;
      (*ohtg_att)[ohtg_idx]= htg_att->GetTuple1(htg_idx);
    }
  }

  float_attributes_htg_to_ohtg_t float_attributes;
};

void openhtg::utils::conversion::convert_openhtg_to_vtkhtg( openHyperTreeGrid & ohtg, vtkUniformHyperTreeGrid* uhtg )
{
  FloatAttributeOHTGToUHTG floatAttributes(ohtg, uhtg);
  initVTKUniformHTGFromOpenHTG(uhtg, ohtg);
  openHyperTreeGridCursorNonOriented ocursor{ohtg};
  OHTGCursorAdaptor adapt_ocursor{ocursor};
  vtkNew<vtkHyperTreeGridNonOrientedCursor> vcursor;

  do {
    const uint32_t treeId = adapt_ocursor.GetTreeIndex();
    const vtkIdType nbElements = uhtg->GetNumberOfVertices();
    std::cout << "Tree index " << treeId << " with offset " << nbElements
              << "\n";
    uhtg->InitializeNonOrientedCursor(vcursor, treeId, true);
    vcursor->SetGlobalIndexStart(nbElements);
    dfsCopyTree(adapt_ocursor, *vcursor, floatAttributes);
    // std::cout << "Cell: " << m_cursor.getGlobalIndex() << " - " <<
    // m_cursor.getLevel() << "\n";

  } while (adapt_ocursor.ToNextTree());
  std::cout << "NbVertices: " << ohtg.getNumberOfNodes() << " vs "
            << uhtg->GetNumberOfVertices() << "\n";
}

void openhtg::utils::conversion::convert_vtkhtg_to_openhtg( vtkHyperTreeGrid* htg, openHyperTreeGrid & ohtg)
{

  FloatAttributeHTGToOHTG floatAttributes(htg, ohtg);
  initOpenHTGFromVTKHyperTreeGrid(htg, ohtg);
  openHyperTreeGridCursorNonOriented ocursor{ohtg};
  OHTGCursorAdaptor adapt_ocursor{ocursor};
  vtkNew<vtkHyperTreeGridNonOrientedCursor> vcursor;

  vtkHyperTreeGrid::vtkHyperTreeGridIterator treeIt;
  treeIt.Initialize(htg);
  vtkIdType treeId = 0;
  while ( treeIt.GetNextTree(treeId))
  {
    htg->InitializeNonOrientedCursor(vcursor, treeId);
    ocursor.toTree(treeId, true);
    dfsCopyTree(*vcursor, adapt_ocursor,floatAttributes);
  }

  std::cout << "NbVertices: " << ohtg.getNumberOfNodes() << " vs "
            << htg->GetNumberOfVertices() << "\n";

}
