//
// Created by antoine on 15/06/2020.
//

#ifndef OPENHTG_DATAGENERATION_H
#define OPENHTG_DATAGENERATION_H

#include <cstdint>

class openHyperTreeGrid;
class dataGeneration
{
public:
	static void generateSphere(openHyperTreeGrid &htg, double center[3], double radius, uint32_t targetLevel, bool traverseBFS,
                                   bool missOneOctant = false);
	static void generateDifferentSizeCubes(openHyperTreeGrid & htg, uint32_t levelDifference);
};


#endif //OPENHTG_DATAGENERATION_H
