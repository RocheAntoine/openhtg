//
// Created by antoine on 15/06/2020.
//

#include "dataGeneration.h"
#include "openHyperTreeGrid.h"
#include "openHyperTreeGridCursor.h"
#include "openHyperTree.h"
#include "openHyperTreeGridCursorIterator.h"

inline bool inCircle(double x, double y, double z, double center[3], double radius2)
{
	const double dx = x - center[0];
	const double dy = y - center[1];
	const double dz = z - center[2];
	return dx * dx + dy * dy + dz * dz <= radius2;
}

inline bool toShow(double bounds[6], double center[3], double radius2, bool missOneOctant)
{
	if (missOneOctant && bounds[0] >= center[0] && bounds[2] >= center[1] && bounds[4] >= center[2])
	{
		return false;
	}
	if (inCircle((bounds[0] + bounds[1]) * 0.5f, (bounds[2] + bounds[3]) * 0.5f, (bounds[4] + bounds[5]) * 0.5f, center,
	             radius2))
	{
		return true;
	}
	if (inCircle(bounds[0], bounds[2], bounds[4], center, radius2))
	{
		return true;
	}
	if (inCircle(bounds[0], bounds[2], bounds[5], center, radius2))
	{
		return true;
	}
	if (inCircle(bounds[0], bounds[3], bounds[4], center, radius2))
	{
		return true;
	}
	if (inCircle(bounds[0], bounds[3], bounds[5], center, radius2))
	{
		return true;
	}
	if (inCircle(bounds[1], bounds[2], bounds[4], center, radius2))
	{
		return true;
	}
	if (inCircle(bounds[1], bounds[2], bounds[5], center, radius2))
	{
		return true;
	}
	if (inCircle(bounds[1], bounds[3], bounds[4], center, radius2))
	{
		return true;
	}
	return inCircle(bounds[1], bounds[3], bounds[5], center, radius2);
}

inline bool toRefine(double bounds[6], double center[3], double radius2)
{
	bool isIn = inCircle((bounds[0] + bounds[1]) * 0.5f, (bounds[2] + bounds[3]) * 0.5f, (bounds[4] + bounds[5]) * 0.5f,
	                     center, radius2);
	if (inCircle(bounds[0], bounds[2], bounds[4], center, radius2) != isIn)
	{
		return true;
	}
	if (inCircle(bounds[0], bounds[2], bounds[5], center, radius2) != isIn)
	{
		return true;
	}
	if (inCircle(bounds[0], bounds[3], bounds[4], center, radius2) != isIn)
	{
		return true;
	}
	if (inCircle(bounds[0], bounds[3], bounds[5], center, radius2) != isIn)
	{
		return true;
	}
	if (inCircle(bounds[1], bounds[2], bounds[4], center, radius2) != isIn)
	{
		return true;
	}
	if (inCircle(bounds[1], bounds[2], bounds[5], center, radius2) != isIn)
	{
		return true;
	}
	if (inCircle(bounds[1], bounds[3], bounds[4], center, radius2) != isIn)
	{
		return true;
	}
	return inCircle(bounds[1], bounds[3], bounds[5], center, radius2) != isIn;
}

void
dfs(openHyperTreeGridCursorOrientedGeometry &cursor, double sphereCenter[3], double radius, uint32_t targetLevel,
    bool missOneOctant)
{
	double bounds[6];
	cursor.getBounds(bounds);
	const double radius2 = radius * radius;
	if (toShow(bounds, sphereCenter, radius2, missOneOctant))
	{
		bool shouldRefine = toRefine(bounds, sphereCenter, radius2);
		cursor.setMask(false);
		cursor.setScalarValue("level", cursor.getLevel());
		cursor.setScalarValue("distanceFromOrigin",
		                      bounds[0] * bounds[0] + bounds[1] * bounds[1] + bounds[2] + bounds[2]);

		if (cursor.getLevel() < targetLevel && shouldRefine)
		{
			cursor.subdivideLeaf();
			for (uint32_t i = 0; i < cursor.getNumberOfChildren(); i++)
			{
				cursor.toChild(i);
				dfs(cursor, sphereCenter, radius2, targetLevel, missOneOctant);
				cursor.toParent();
			}
		}


	}
}

void dataGeneration::generateSphere(openHyperTreeGrid &htg, double center[3], double radius, uint32_t targetLevel,
                                    bool traverseBFS,
                                    bool missOneOctant)
{
	const double radius2 = radius * radius;
	openHyperTreeGridCursorOrientedGeometry cursor(htg);
	htg.addScalarField("level");
	htg.addScalarField("distanceFromOrigin");

	treeTraverser<openHyperTreeGridCursorOrientedGeometry> traverser;
	if (traverseBFS)
	{ traverser.setTraversalModeToBFS(); }
	else
	{ traverser.setTraversalModeToDFS(); }

	for (uint32_t i = 0; i < htg.getMaxNumberOfTrees(); ++i)
	{
		cursor.toTree(i, true);
		traverser.setRoot(cursor);

		for (auto it = traverser.begin(); it != traverser.end(); ++it)
		{
			auto &cursorRef = it.getCursor();
			double bounds[6];
			cursorRef.getBounds(bounds);

			if (toShow(bounds, center, radius2, missOneOctant))
			{
				cursorRef.setMask(false);
				cursorRef.setScalarValue("level", cursorRef.getLevel());
				cursorRef.setScalarValue("distanceFromOrigin",
				                         bounds[0] * bounds[0] + bounds[1] * bounds[1] + bounds[2] * bounds[2]);

				if (cursorRef.getLevel() < targetLevel)
				{
					if (toRefine(bounds, center, radius2))
					{
						cursorRef.subdivideLeaf();
					}
				}
			}
		}
	}
}

namespace {
	template <class Cursor>
	void subdivideLeafAndSetLevel( Cursor & cursor)
	{
    cursor.subdivideLeaf();
		for ( unsigned char i = 0 ; i < cursor.getNumberOfChildren() ; ++i)
		{
			cursor.toChild(i);
			cursor.setScalarValue("level", cursor.getLevel());
			cursor.toParent();
		}
	}
}

void dataGeneration::generateDifferentSizeCubes(openHyperTreeGrid &htg, uint32_t levelDifference)
{
	openHyperTreeGridCursorNonOriented cursor(htg);
	htg.addScalarField("level");

	// Level0 = 1 node
	cursor.toTree(0, true);
	cursor.setMask(false);
	cursor.setScalarValue("level", cursor.getLevel());
	subdivideLeafAndSetLevel(cursor);

	// Level1 = 8 nodes, id 0 unmasked, id 1 subdivided until levelDiff attained
	cursor.toChild(0);
	cursor.setMask(false);
	cursor.toParent();
	
	cursor.toChild(1);
	cursor.setMask(false);

	for (uint32_t i = 0; i < levelDifference; ++i)
	{
		subdivideLeafAndSetLevel(cursor);
		cursor.toChild(0);
		cursor.setMask(false);
	}
}
