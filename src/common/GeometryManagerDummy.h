//
// Created by duboisj on 18/06/2020.
//

#ifndef OPENHTG_GEOMETRYMANAGER_DUMMY_H
#define OPENHTG_GEOMETRYMANAGER_DUMMY_H

#include <cstdint>

class GeometryManagerDummy {
public:
  template<class NodeType>
  void setCurrentNodeMinBoundingBox( NodeType& node, double minBoundX, double minBoundY, double minBoundZ ) {
  }
  template<class NodeType>
  void moveCurrentNodeMinBoundingBox( NodeType& node, uint32_t childIndex, const double * scale){
  }

  template<class NodeType>
  void computeBounds(const NodeType & node, const double * scale, double bounds[6])
  {
  }
};

#endif // OPENHTG_GEOMETRYMANAGER_DUMMY_H
