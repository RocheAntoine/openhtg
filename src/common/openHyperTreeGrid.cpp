//
// Created by antoine on 25/05/2020.
//

#include "openHyperTreeGrid.h"
#include "openHyperTree.h"
#include <iostream>


openHyperTreeGrid::openHyperTreeGrid()
{
	numberOfHyperTreePerAxis = {0, 0, 0};
	numberOfNodes = 0;
}
/*
bool openHyperTreeGrid::insertTree(std::shared_ptr<openHyperTree> hyperTree, uint32_t index)
{
	auto result = hyperTrees.insert(std::make_pair(index, hyperTree));
	if (!result.second)
	{
		return false;
	}

	hyperTree->setTreeIndex(index);

	auto currentHyperTree = result.first;

	if (currentHyperTree != hyperTrees.begin())
	{
		uint64_t offset;
		currentHyperTree--;
		offset = currentHyperTree->second->getScalarOffset() + currentHyperTree->second->getNumberOfNodes();
		currentHyperTree++;
		currentHyperTree->second->setScalarOffset(offset);
	}
	return true;
}*/

std::map<uint32_t, std::shared_ptr<openHyperTree>>::iterator openHyperTreeGrid::getTree(uint32_t index, bool create)
{
	if (index >= this->getMaxNumberOfTrees())
	{
		return this->getEndTreeIterator();
	}
	auto currentHyperTree = this->hyperTrees.find(index);
	if (create && currentHyperTree == this->getEndTreeIterator())
	{
		auto hyperTree = std::make_shared<openHyperTree>(index);
		hyperTree->setScalarOffset(numberOfNodes);
		auto p = this->hyperTrees.insert(std::make_pair(index, hyperTree));
		if (!p.second)
		{
			return this->getEndTreeIterator();
		}
		return p.first;
	}
	else
	{
		return currentHyperTree;
	}
}

void openHyperTreeGrid::getBounds(double *bounds)
{
	bounds[0] = minBoundingBox[0];
	bounds[1] = maxBoundingBox[0];
	bounds[2] = minBoundingBox[1];
	bounds[3] = maxBoundingBox[1];
	bounds[4] = minBoundingBox[2];
	bounds[5] = maxBoundingBox[2];
}

bool openHyperTreeGrid::isEqualTo(openHyperTreeGrid &htgIn)
{
	if (numberOfHyperTreePerAxis != htgIn.numberOfHyperTreePerAxis)
	{
		return false;
	}
	if (numberOfNodes != htgIn.numberOfNodes)
	{
		return false;
	}

	for (uint32_t i = 0; i < 3; i++)
	{
		if (minBoundingBox[i] != htgIn.minBoundingBox[i])
		{
			return false;
		}

		if (maxBoundingBox[i] != htgIn.maxBoundingBox[i])
		{
			return false;
		}
	}

	auto ht1 = hyperTrees.begin();
	auto ht2 = htgIn.hyperTrees.begin();

	for (; ht1 != hyperTrees.end(); ht1++, ht2++)
	{
		if (ht1->first != ht2->first)
		{
			return false;
		}

		if (ht1->second->getNumberOfNodes() != ht2->second->getNumberOfNodes())
		{
			return false;
		}
		if (ht1->second->getScalarOffset() != ht2->second->getScalarOffset())
		{
			return false;
		}


          std::vector<uint32_t> &elderChild1 = ht1->second->getElderChildIndex();
          std::vector<uint32_t> &elderChild2 = ht2->second->getElderChildIndex();
		if (elderChild1.size() != elderChild2.size())
		{
                  return false;
                }
		const uint32_t vectorSize = elderChild1.size();
		for (uint32_t i = 0; i < vectorSize; ++i)
		{
			if (elderChild1[i] != elderChild2[i])
			{
				return false;
			}
		}
	}

	if(scalars != htgIn.scalars)
		return false;

//	auto it = scalars.begin();
//
//
//	for (; it != scalars.end(); ++it)
//	{
////		auto type = decltype(it->second[0]);
//		auto scalarIn = htgIn.getscalarArray(it->first);
//		for (uint32_t i = 0; i < numberOfNodes; ++i)
//		{
//			if (std::any_cast<double>(it->second[i]) != std::any_cast<double>(scalarIn[i]))
//			{
//				return false;
//			}
//		}
//	}

	for (uint32_t i = 0; i < numberOfNodes / 8 + 1; ++i)
	{
		if (bitMask[i] != htgIn.bitMask[i])
		{
			return false;
		}
	}

	return true;

}

uint32_t openHyperTreeGrid::getNumberOfNodes() const
{
	return numberOfNodes;
}

void openHyperTreeGrid::setDimensions(uint32_t dimension[3])
{
	numberOfHyperTreePerAxis = {dimension[0], dimension[1], dimension[2]};
}

void openHyperTreeGrid::setDimensions(uint32_t a, uint32_t b, uint32_t c)
{
	numberOfHyperTreePerAxis = {a, b, c};
}

void openHyperTreeGrid::getDimensions(uint32_t dimension[3])
{
	uint32_t *tmp = numberOfHyperTreePerAxis.data();
	dimension[0] = tmp[0];
	dimension[1] = tmp[1];
	dimension[2] = tmp[2];
}

void openHyperTreeGrid::getHyperTreeSize(double size[3])
{
	size[0] = (maxBoundingBox[0] - minBoundingBox[0]) / numberOfHyperTreePerAxis[0];
	size[1] = (maxBoundingBox[1] - minBoundingBox[1]) / numberOfHyperTreePerAxis[1];
	size[2] = (maxBoundingBox[2] - minBoundingBox[2]) / numberOfHyperTreePerAxis[2];
}

std::vector<float> &openHyperTreeGrid::getscalarArray(const std::string &fieldName)
{
	auto it = scalars.find(fieldName);
	if (it == scalars.end())
	{
		std::cerr << "Error : Could not find " << fieldName << " scalar field" << std::endl;
	}
	return it->second;
}

std::vector<uint8_t> &openHyperTreeGrid::getBitMask()
{
	return bitMask;
}

uint32_t openHyperTreeGrid::getMaxNumberOfTrees()
{
	return numberOfHyperTreePerAxis[0] * numberOfHyperTreePerAxis[1] * numberOfHyperTreePerAxis[2];
}

void openHyperTreeGrid::setBoundingBox(const double minBB[3],const double maxBB[3])
{
	minBoundingBox = {minBB[0], minBB[1], minBB[2]};
	maxBoundingBox = {maxBB[0], maxBB[1], maxBB[2]};
	scaleArray.clear();
}

void openHyperTreeGrid::setBitMaskValue(uint32_t index, bool masked)
{
	if (masked)
	{
		bitMask[index / 8] |= (0x80u >> (index % 8));
	}
	else
	{
		uint8_t tmp = -1;
		tmp ^= 0x80u >> (index % 8);
		bitMask[index / 8] &= tmp;
	}
}

bool openHyperTreeGrid::getBitMaskValue(uint32_t index)
{
	return (bitMask[index / 8] & (0x80u >> (index % 8))) != 0;
}

void openHyperTreeGrid::setScalarValue(const std::string &fieldName, uint32_t index, float value)
{
	auto it = scalars.find(fieldName);
	if (it == scalars.end())
	{
		std::cerr << "Error : Could not find " << fieldName << " scalar field" << std::endl;
	}
	it->second[index] = value;
}
/*
float openHyperTreeGrid::getScalarValue(const std::string &fieldName, uint32_t index)
{
	auto it = scalars.find(fieldName);
	if (it == scalars.end())
	{
		std::cerr << "Error : Could not find " << fieldName << " scalar field" << std::endl;
	}
	return it->second[index];
}*/

void openHyperTreeGrid::addNodes(uint32_t nbNodes)
{
	numberOfNodes += nbNodes;
	for (auto & scalar : scalars)
	{
		scalar.second.resize(numberOfNodes);
	}
	bitMask.resize(numberOfNodes / 8 + 1, 0xFF);
}

std::map<uint32_t, std::shared_ptr<openHyperTree>>::iterator openHyperTreeGrid::getBeginTreeIterator()
{
	return hyperTrees.begin();
}

std::map<uint32_t, std::shared_ptr<openHyperTree>>::iterator openHyperTreeGrid::getEndTreeIterator()
{
	return hyperTrees.end();
}

void openHyperTreeGrid::addScalarField(const std::string& fieldName)
{
	scalars.insert(std::make_pair(fieldName, std::vector<float>(numberOfNodes)));
}

const double* openHyperTreeGrid::getScale(uint32_t level)
{
	return scaleArray[level].data();
}

void openHyperTreeGrid::setScale(uint32_t level)
{
	if(level >= scaleArray.size())
	{
		if(level == 0)
		{
			double treeSize[3];
			getHyperTreeSize(treeSize);
			scaleArray.emplace_back(std::array<double, 3>{treeSize[0], treeSize[1], treeSize[2]});
		}
		else
		{
			const double *scale = getScale(level - 1);
			scaleArray.emplace_back(std::array<double, 3>{scale[0] * 0.5f, scale[1] * 0.5f, scale[2] * 0.5f});
		}
	}
}

uint32_t openHyperTreeGrid::getNumberOfLevels()
{
	return scaleArray.size();
}