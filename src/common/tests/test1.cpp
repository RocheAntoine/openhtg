#include "openHyperTree.h"
#include "openHyperTreeGrid.h"
#include "openHyperTreeGridCursor.h"
#include "openHyperTreeGridCursorIterator.h"

#include <iostream>
#include <string>

void printBounds(double bounds[6])
{
	for (int i = 0; i < 6; i++)
		std::cout << bounds[i] << "  ";
	std::cout << std::endl;
}

void testCreateHyperTree(openHyperTreeGrid &htg)
{
	openHyperTreeGridCursorNonOriented cursor(htg);
	uint32_t dimensions[3];
	htg.getDimensions(dimensions);

	for (int k = 0; k < dimensions[2]; k++)
	{
		for (int j = 0; j < dimensions[1]; j++)
		{
			for (int i = 0; i < dimensions[0]; i++)
			{
				if (!cursor.toTree(k * dimensions[0] * dimensions[1] + j * dimensions[0] + i, true))
				{
					std::cerr << "Failed to create HyperTree" << std::endl;
					exit(EXIT_FAILURE);
				}
			}
		}
	}
}

void testCreateHyperTreeOutOfDimensions(openHyperTreeGrid &htg)
{
	openHyperTreeGridCursorNonOriented cursor(htg);
	uint32_t numberOfTrees = htg.getMaxNumberOfTrees();
	if (cursor.toTree(numberOfTrees, true))
	{
		std::cerr << "Should not create a tree when it is out of dimensions" << std::endl;
		exit(EXIT_FAILURE);
	}
}

void testCursorCreateFirstTree(openHyperTreeGrid &htg)
{
	openHyperTreeGridCursorNonOriented cursor(htg);
	if (!cursor.toTree(0, true))
	{
		std::cerr << "Failed to create the first HyperTree" << std::endl;
	}
}

void testCursorCreateToTree(openHyperTreeGrid &htg)
{
	openHyperTreeGridCursorNonOriented cursor(htg);

	for (uint32_t i = 0; i < htg.getMaxNumberOfTrees(); ++i)
	{
		if (!cursor.toTree(i, true))
		{
			std::cerr << "Could not create hyperTree" << std::endl;
			exit(EXIT_FAILURE);
		}
	}
}

void testCursorToNextTreeFull(openHyperTreeGrid &htg)
{
	openHyperTreeGridCursorNonOriented cursor(htg);

	for (uint32_t i = 0; i < htg.getMaxNumberOfTrees(); ++i)
	{
		cursor.toTree(i, true);
	}

	uint32_t nbTree = 0;

	cursor.toTree(0);

	do
	{
		nbTree++;
	} while (cursor.toNextTree());

	if (nbTree != htg.getMaxNumberOfTrees())
	{
		std::cerr << "Number of iterated trees is not correct" << std::endl;
	}
}

void testCursorToFirstTreeEmpty(openHyperTreeGrid &htg)
{
	openHyperTreeGridCursorNonOriented cursor(htg);
	if (cursor.toTree(0))
	{
		std::cerr << "Cursor should not be created when there is no Tree" << std::endl;
		exit(EXIT_FAILURE);
	}
}

void testCursorRootIndex(openHyperTreeGrid &htg)
{
	openHyperTreeGridCursorNonOriented cursor(htg);
	cursor.toTree(0, true);
	if (cursor.getNodeIndex() != 0)
	{
		std::cerr << "Root index should be 0" << std::endl;
		exit(EXIT_FAILURE);
	}
}

template <class T>
bool checkBounds(const T & b1, const T& b2)
{
  for ( uint8_t i: {0, 2, 4})
  {
    const double width1 = b1[1] - b1[0];
    const double width2 = b2[1] - b2[0];
    if ( width2 != (width1 / 2) )
    {
      std::cerr << "Incorrect width after subdivide:" << width1 << " vs " << width2 << std::endl;
      exit(EXIT_FAILURE);
      return false;
    }
  }
  return true;
}

void testCursorSubdivise(openHyperTreeGrid &htg)
{
  {
    openHyperTreeGridCursorNonOriented cursor(htg);
    cursor.toTree(0, true);
    cursor.subdivideLeaf();
    for (int i = 0; i < cursor.getNumberOfChildren(); i++) {
      cursor.toChild(i);
      if (cursor.getNodeIndex() != i + 1) {
        std::cerr << "Incorrect node index" << std::endl;
        exit(EXIT_FAILURE);
      }
      cursor.toParent();
    }
    cursor.toChild(4);
    for (uint32_t i = 0; i < 10; ++i) {
      cursor.subdivideLeaf();
      cursor.toChild(i % cursor.getNumberOfChildren());
      if (cursor.getLevel() != i + 2) {
        std::cerr << "Incorrect node level" << std::endl;
        exit(EXIT_FAILURE);
      }
    }
  }
  {
    openHyperTreeGridCursorOrientedGeometry cursor(htg);
    cursor.toTree(0);
    double bounds1[6], bounds2[6];
    cursor.getBounds(bounds1);
    printBounds(bounds1);
    cursor.toChild(4);
    cursor.getBounds(bounds2);
    printBounds(bounds2);
    checkBounds(bounds1, bounds2);
    uint32_t lev = 0;
    double* ptr1 = bounds1, *ptr2 = bounds2;
    do {
      cursor.toChild(lev % cursor.getNumberOfChildren());
      cursor.getBounds(ptr1);
      printBounds(ptr1);
      checkBounds(ptr2, ptr1);
      std::swap(ptr1, ptr2);
      ++lev;
    } while (!cursor.isLeaf());
  }
}

void testCursorIterator(openHyperTreeGrid &htg)
{
  openHyperTreeGridCursorNonOriented htgCursor(htg);
	htgCursor.toTree(0, true);

	for (uint32_t i = 0; i < 5; ++i)
	{
		htgCursor.subdivideLeaf();
		htgCursor.toChild(0);
	}


	treeTraverser<openHyperTreeGridCursorNonOriented> traverser ;


	openHyperTreeGridCursorNonOriented cursor(htg);
	cursor.toTree(0);
	traverser.setRoot(cursor);
//	traverser.setTraversalModeToDFS();
        uint8_t idx = 0;
	for (auto it = traverser.begin(); it != traverser.end(); ++it)
	{
          auto &mcursor = it.getCursor();
          std::cout << "Index : " << mcursor.getNodeIndex() << "\tLevel : " << mcursor.getLevel() << std::endl;
          if (mcursor.getNodeIndex() != idx)
          {
            std::cerr << "Incorrect node index" << std::endl;
            exit(EXIT_FAILURE);
          }
          if ( ( idx == 0 && mcursor.getLevel() != 0) || ( idx > 0 && (mcursor.getLevel()-1) != ((idx-1) / 8) ))
          {
            std::cerr << "Incorrect node level" << std::endl;
            exit(EXIT_FAILURE);
          }
                ++idx;
	}
}

template<class T>
void display_type(const T &v)
{
	std::cout << v;
}

#include <bitset>

void display_type(const uint8_t &v)
{
	std::bitset<8> bs(v);
	std::cout << bs.to_string();
}

template<class T>
void display_container(const std::string &s, const T &cont)
{
	if (!s.empty())
	{ std::cout << s << std::endl; }
	for (size_t i = 0; i < cont.size(); ++i)
	{
		display_type(cont.at(i));
		std::cout << " ";
	}
	if (cont.size())
	{ std::cout << std::endl; }
}


void testElderChildIndex(openHyperTreeGrid &htg)
{
	openHyperTreeGridCursorNonOriented cursor(htg);
	const uint8_t tid = 1;
	cursor.toTree(tid, true);
	auto *tree = cursor.getHyperTree();

	display_container("Empty HTG", htg.getBitMask());
	display_container("", tree->getElderChildIndex());
	cursor.setMask(false);
	//ElderChildIndex: a
	//bitmask: 0
	display_container("HTG 1 voxel", htg.getBitMask());
	display_container("", tree->getElderChildIndex());
	//level 0, 0
//	subdivideAndInitMaskChildrenTrue(cursor);
	cursor.subdivideLeaf();

	//ElderChildIndex: 1
	//bitmask: 0 11111111 (0000000)
	display_container("HTG 1 voxel - 1 level", htg.getBitMask());
	display_container("", tree->getElderChildIndex());
	cursor.toChild(3);
	//level 1, 0.3
	cursor.setMask(false);
	display_container("HTG 2 voxels - 1 level", htg.getBitMask());
	display_container("", tree->getElderChildIndex());
	//bitmask: 0 11101111 (0000000)
//	subdivideAndInitMaskChildrenTrue(cursor);
	cursor.subdivideLeaf();

	//ElderChildIndex: 1 aaa9
	//bitmask: 0 11101111 11111111 (0000000)
	display_container("HTG 2 voxels - 2 levels", htg.getBitMask());
	display_container("", tree->getElderChildIndex());
	cursor.toChild(1);
	//level 2, 0.3.1
	cursor.setMask(false);
	//bitmask: 0 11101111 10111111 (0000000)
	display_container("HTG 3 voxels - 2 levels", htg.getBitMask());
	display_container("", tree->getElderChildIndex());
	cursor.toParent();
	//level 1, 0.3
	cursor.toParent();
	//level 0, 0
	cursor.toChild(0);
	//level 1, 0.0
	cursor.setMask(false);
	//bitmask: 0 01101111 10111111 (0000000)
	display_container("HTG 4 voxels - 2 levels", htg.getBitMask());
	display_container("", tree->getElderChildIndex());
//	subdivideAndInitMaskChildrenTrue(cursor);
	cursor.subdivideLeaf();

	//ElderChildIndex: 1 17-aa9 
	//bitmask: 0 01101111 10111111 11111111 (0000000)
	display_container("HTG 4 voxels - 2 levels - sub", htg.getBitMask());
	display_container("", tree->getElderChildIndex());
	cursor.toChild(7);
	cursor.setMask(false);
	//bitmask: 0 01101111 10111111 11111110 (0000000)
	display_container("HTG 5 voxels - 2 levels - sub", htg.getBitMask());
	display_container("", tree->getElderChildIndex());


	const std::vector<std::string> expected_bitmask = {"00110111", "11011111", "11111111", "01111111"};
	uint8_t i = 0;
	for (const std::string &s: expected_bitmask)
	{
		const std::string bm = std::bitset<8>(htg.getBitMask().at(i)).to_string();
		std::cout << bm;
		if (s.compare(bm))
		{
			exit(EXIT_FAILURE);
		}
		++i;
	}
	std::cout << std::endl;
	const std::vector<uint32_t> expected_ElderChildIndex = {1, 17, 4294967295, 4294967295, 9};
	if (expected_ElderChildIndex.size() - tree->getElderChildIndex().size())
	{
		std::cout << "ElderChildIndex has wrong size" << std::endl;
		std::cout << "had " << tree->getElderChildIndex().size() << " but expected " << expected_ElderChildIndex.size()
		          << std::endl;
		exit(EXIT_FAILURE);
	}
	i = 0;
	for (const auto &v : expected_ElderChildIndex)
	{
		if (tree->getElderChildIndex().at(i) != v)
		{
			exit(EXIT_FAILURE);
		}
		++i;
	}
}

void verifScale(double supposedScale[3], double bounds[6])
{
	for(uint8_t i = 0; i < 3; ++i)
	{
		if(supposedScale[i] != (bounds[i * 2 + 1] - bounds[i * 2]))
		{
			std::cerr << "Error : scale is not correct" << std::endl;
			exit(EXIT_FAILURE);
		}
	}
}

void testScale(openHyperTreeGrid & htg)
{
	uint32_t dimensions[3];
	htg.getDimensions(dimensions);
	double bounds[6];
	double cursorBounds[6];
	htg.getBounds(bounds);
	double supposedScale[3] = {(bounds[1] - bounds[0]) / dimensions[0], (bounds[3] - bounds[2]) / dimensions[1], (bounds[5] - bounds[4]) / dimensions[2]};

	openHyperTreeGridCursorNonOrientedGeometry cursor(htg);

	cursor.toTree(0, true);

	cursor.getBounds(cursorBounds);

	verifScale(supposedScale, cursorBounds);

	for(uint8_t i = 0; i < 10; ++i)
	{
		cursor.subdivideLeaf();
		cursor.toChild(0);

		supposedScale[0] /= 2;
		supposedScale[1] /= 2;
		supposedScale[2] /= 2;

		cursor.getBounds(cursorBounds);
		verifScale(supposedScale, cursorBounds);
	}

	for(uint8_t i = 0; i < 10; ++i)
	{
		cursor.toParent();

		supposedScale[0] *= 2;
		supposedScale[1] *= 2;
		supposedScale[2] *= 2;

		cursor.getBounds(cursorBounds);
		verifScale(supposedScale, cursorBounds);
	}
}

int main(int argc, char *argv[])
{
	if (argc < 5)
	{
		return 1;
	}

	openHyperTreeGrid htg;
	double minBounds[3] = {0, 0, 0};
	double maxBounds[3] = {100, 100, 100};
	htg.setBoundingBox(minBounds, maxBounds);
	uint32_t dimensions[3] = {static_cast<uint32_t>(atoi(argv[1])), static_cast<uint32_t>(atoi(argv[2])),
	                          static_cast<uint32_t>(atoi(argv[3]))};
	htg.setDimensions(dimensions);
	htg.addScalarField("scalar");
	uint32_t testNumber = atoi(argv[4]);

	switch (testNumber)
	{
		case (0):
			testCreateHyperTree(htg);
			break;
		case (1):
			testCreateHyperTreeOutOfDimensions(htg);
			break;
		case (2):
			testCursorCreateFirstTree(htg);
			break;
		case (3):
			testCursorCreateToTree(htg);
			break;
		case (4):
			testCursorToFirstTreeEmpty(htg);
			break;
		case (5):
			testCursorToNextTreeFull(htg);
			break;
		case (6):
			testCursorRootIndex(htg);
			break;
		case (7):
			testCursorSubdivise(htg);
			break;
		case (8):
			testElderChildIndex(htg);
			break;
		case (9):
			testCursorIterator(htg);
			break;
		case(10):
			testScale(htg);
		default:
			break;
	}


	return 0;
}
