//
// Created by duboisj on 18/06/2020.
//

#ifndef OPENHTG_HISTORYMANAGER_DUM_H
#define OPENHTG_HISTORYMANAGER_DUM_H

template <class T>
class CurrentNodeManager;

template <class T>
class HistoryManagerDummy{
public:
  typedef T NodeType;
  void clearHistory() {
  }

  void addElement(const T & element) {
  }

  void toParent(CurrentNodeManager<T> & currentNodeManager) {
  }

  void copyHistoryStack(const HistoryManagerDummy<T> & other)
  {
  }
};

#endif // OPENHTG_HISTORYMANAGER_DUM_H
