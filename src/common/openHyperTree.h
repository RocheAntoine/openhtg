//
// Created by antoine on 25/05/2020.
//

#ifndef SERIALIZED_HTG_SERIALIZEDHYPERTREE_H
#define SERIALIZED_HTG_SERIALIZEDHYPERTREE_H

#include <cstdint>
#include <vector>
#include <array>
//#include "cereal/types/vector.hpp"
//#include "cereal/archives/binary.hpp"
//#include "cereal/access.hpp"

class openHyperTreeGrid;

class openHyperTree
{

public:

	explicit openHyperTree();
	explicit openHyperTree(uint32_t);
	openHyperTree(const openHyperTree &);

	void setTreeIndex(uint32_t);
	void setScalarOffset(uint32_t);
	void setNumberOfNodes(uint32_t);
	void setElderChildIndex(const std::vector<uint32_t>&);
	void addNodes(uint32_t, uint32_t);

	[[nodiscard]] uint32_t getTreeIndex() const;
	[[nodiscard]] uint64_t getScalarOffset() const;
	[[nodiscard]] uint32_t getNumberOfNodes() const;
	std::vector<uint32_t> & getElderChildIndex();
	uint32_t getElderChildIndexValue(uint32_t);

	template<class Archive>
	void serialize(Archive & archive)
	{
		archive(numberOfNodes);
		archive(scalarOffset);
		archive(treeIndex);
		archive(elderChildIndex);
	}

private:

//	friend class cereal::access;

	uint32_t treeIndex;
	uint64_t scalarOffset;
	uint32_t numberOfNodes;

	std::vector<uint32_t> elderChildIndex;
};


#endif //SERIALIZED_HTG_SERIALIZEDHYPERTREE_H
