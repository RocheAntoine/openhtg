//
// Created by duboisj on 18/06/2020.
//

#ifndef OPENHTG_HISTORYMANAGER_H
#define OPENHTG_HISTORYMANAGER_H

#include "CurrentNodeManager.h"

#include <cassert>
#include <iostream>
#include <stack>

template <class T>
class HistoryManager{
public:
  typedef T NodeType;
  void clearHistory() {
    this->historyStack = std::stack<T>();
  }

  void addElement(const T & element) {
    this->historyStack.push(element);
  }

  void toParent(CurrentNodeManager<T> & currentNodeManager) {
    if (!historyStack.empty())
    {
      currentNodeManager.setCurrentNode(historyStack.top());
      historyStack.pop();
    }
    else
    {
      std::cerr << "toParent() impossible, already at root" << std::endl;
      assert(0);
    }
    currentNodeManager.decrementLevel();
  }

  void copyHistoryStack(const HistoryManager<T> & other)
  {
    setHistoryStack(other.getHistoryStack());
  }


private:
  std::stack<T> historyStack;

public:
  const std::stack<T> &getHistoryStack() const { return historyStack; }
  void setHistoryStack(const std::stack<T> &mhistoryStack) {
    HistoryManager::historyStack = mhistoryStack;
  }
};

#endif // OPENHTG_HISTORYMANAGER_H
