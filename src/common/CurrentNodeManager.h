//
// Created by duboisj on 18/06/2020.
//

#ifndef OPENHTG_CURRENTNODEMANAGER_H
#define OPENHTG_CURRENTNODEMANAGER_H

#include <cstdint>

template< class T>
class CurrentNodeManager {
public:
  const T &getCurrentNode() const { return currentNode; }
  T &getCurrentNode() { return currentNode; }

  void setCurrentNode(const T & other){
    currentNode = other;
  }

  void incrementLevel() { ++level;}
  void decrementLevel() { --level;}
  void resetLevel() { level=0;}

  void setCurrentNodeIndex( uint32_t nid) {
    currentNode.index = nid;
  }

  [[nodiscard]] uint32_t getCurrentNodeIndex() const { return getCurrentNode().index;}

private:
  T currentNode;
  uint8_t level = 0;

public:
  void setLevel(uint8_t mlevel) { CurrentNodeManager::level = mlevel; }
  [[nodiscard]] uint8_t getLevel() const { return CurrentNodeManager::level; }

};

#endif // OPENHTG_CURRENTNODEMANAGER_H
