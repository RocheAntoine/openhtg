//
// Created by antoine on 28/05/2020.
//

#include <iostream>
#include "openHyperTreeGridVTKSerialization.h"
#include "openHyperTree.h"
#include "openHyperTreeGrid.h"
#include "dataGeneration.h"

#define TARGET_LEVEL 6
#define CIRCLE_RADIUS 50

double circleCenter[3] = {CIRCLE_RADIUS, CIRCLE_RADIUS, CIRCLE_RADIUS};


int main()
{
	openHyperTreeGrid htg;

	double minBounds[3] = {0, 0, 0};
	double maxBounds[3] = {CIRCLE_RADIUS * 2, CIRCLE_RADIUS * 2, CIRCLE_RADIUS * 2};
	htg.setBoundingBox(minBounds, maxBounds);
	uint32_t dimensions[3] = {1, 1, 1};
	htg.setDimensions(dimensions);

	dataGeneration::generateSphere(htg, circleCenter, CIRCLE_RADIUS, 7, false);

	openhtg::io::mvtk::writeInFile("sphere.htg", htg);
	openHyperTreeGrid htg2;
	openhtg::io::mvtk::loadFromFile("sphere.htg", htg2);
        // htg and htg2 are equal because we generate htg in DFS, and convert it in DFS as well
        // should generateSphere be called with traverseBFS to true, then test would fail
	if (!htg.isEqualTo(htg2))
	{
		std::cerr << "Created HTG and read / written one are not the same" << std::endl;
		exit(EXIT_FAILURE);
	}
	return 0;
}
