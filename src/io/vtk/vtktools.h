//
// Created by duboisj on 03/07/2020.
//

#ifndef OPENHTG_VTKTOOLS_H
#define OPENHTG_VTKTOOLS_H

#include <string>
class vtkHyperTreeGrid;
class vtkUniformHyperTreeGrid;

namespace openhtg {
namespace io {
namespace vtk {
void writeVTKHTGToFile(vtkUniformHyperTreeGrid * uhtg, const std::string & filename);
void loadVTKHTGFromFile(vtkHyperTreeGrid * htg, const std::string & filename);
}
}
}
#endif // OPENHTG_VTKTOOLS_H
