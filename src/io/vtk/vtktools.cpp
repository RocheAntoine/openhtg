//
// Created by duboisj on 03/07/2020.
//
#include "vtktools.h"
#include "vtkHyperTreeGrid.h"
#include "vtkNew.h"
#include "vtkUniformHyperTreeGrid.h"
#include "vtkXMLHyperTreeGridWriter.h"
#include <vtkXMLHyperTreeGridReader.h>

void openhtg::io::vtk::writeVTKHTGToFile(vtkUniformHyperTreeGrid * uhtg, const std::string & filename)
{
  vtkNew<vtkXMLHyperTreeGridWriter> htg_writer;
  htg_writer->SetDataSetMajorVersion(1);
  htg_writer->SetDataModeToAppended();
  htg_writer->SetInputData(uhtg);
  htg_writer->SetFileName(filename.c_str());
  std::cout << "Write to " << filename << "\n";
  htg_writer->Write();
}

void openhtg::io::vtk::loadVTKHTGFromFile(vtkHyperTreeGrid * htg, const std::string & filename)
{
  vtkNew<vtkXMLHyperTreeGridReader> htg_reader;
  htg_reader->SetFileName(filename.c_str());

  //htg_reader->SetFixedLevel(fixedlevel);
  htg_reader->Update();
  htg->Initialize();
  auto* htg_reader_data = htg_reader->GetOutput();
  htg->ShallowCopy(htg_reader_data);
}