//
// Created by antoine on 25/05/2020.
//
#include "openHyperTreeGridVTKSerialization.h"

#include <iostream>
#include "vtkUniformHyperTreeGrid.h"
#include "vtkXMLHyperTreeGridWriter.h"
#include "vtk_conversion.h"
#include "vtktools.h"

void openhtg::io::mvtk::writeInFile(const std::string &filename, openHyperTreeGrid & ohtg)
{
  vtkNew<vtkUniformHyperTreeGrid> uhtg;
  openhtg::utils::conversion::convert_openhtg_to_vtkhtg(ohtg, uhtg);
  openhtg::io::vtk::writeVTKHTGToFile(uhtg, filename);
}

void openhtg::io::mvtk::loadFromFile(const std::string &filename, openHyperTreeGrid & ohtg)
{
  vtkNew<vtkHyperTreeGrid> htg;
  openhtg::io::vtk::loadVTKHTGFromFile(htg, filename);
  openhtg::utils::conversion::convert_vtkhtg_to_openhtg(htg, ohtg);
}
