//
// Created by antoine on 25/05/2020.
//

#ifndef OPEN_HTG_VTK_WRITER_H

#include <string>

class openHyperTreeGrid;

namespace openhtg
{
	namespace io
	{
		namespace mvtk
		{
			void writeInFile(const std::string &filename, openHyperTreeGrid &htg);

			void loadFromFile(const std::string &filename, openHyperTreeGrid &htg);
		}
	}
}

#endif //OPEN_HTG_VTK_WRITER_H
