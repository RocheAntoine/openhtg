#include <iostream>

#include "openHyperTreeGridCerealSerialization.h"
#include "openHyperTreeGrid.h"
#include "openHyperTreeGridCursor.h"

void testFileWriting(openHyperTreeGrid &htg)
{
	openHyperTreeGridCursorNonOriented cursor(htg);

	for (uint32_t i = 0; i < htg.getMaxNumberOfTrees(); ++i)
	{
		cursor.toTree(i, true);
		cursor.setMask(false);
		cursor.setScalarValue("scalar", cursor.getTreeIndex());
		cursor.subdivideLeaf();
		for (uint32_t j = 0; j < cursor.getNumberOfChildren(); ++j)
		{
			cursor.toChild(j);
			cursor.setMask(j % 2);
			cursor.setScalarValue("scalar", j);
			cursor.toParent();
		}
	}
	openhtg::io::mcereal::writeInFile("test.shtg", htg);
	openHyperTreeGrid htg2;
	openhtg::io::mcereal::loadFromFile("test.shtg", htg2);
	if (!htg.isEqualTo(htg2))
	{
		std::cerr << "Original HTG and written - read one are not the same" << std::endl;
		exit(EXIT_FAILURE);
	}
	double bounds[6];
	htg.getBounds(bounds);
	for (uint32_t i = 0; i < 6; ++i)
	{
		std::cout << bounds[i] << " ";
	}
	std::cout << std::endl;

	htg2.getBounds(bounds);
	for (uint32_t i = 0; i < 6; ++i)
	{
		std::cout << bounds[i] << " ";
	}
	std::cout << std::endl;
}

int main(int argc, char *argv[])
{
	if (argc < 5)
	{
		return 1;
	}

	openHyperTreeGrid htg;
	double minBounds[3] = {0, 0, 0};
	double maxBounds[3] = {100, 100, 100};
	htg.setBoundingBox(minBounds, maxBounds);
	uint32_t dimensions[3] = {static_cast<uint32_t>(atoi(argv[1])), static_cast<uint32_t>(atoi(argv[2])),
	                          static_cast<uint32_t>(atoi(argv[3]))};
	htg.setDimensions(dimensions);

	htg.addScalarField("scalar");

	uint32_t testNumber = atoi(argv[4]);

	switch (testNumber)
	{
		case (0):
			testFileWriting(htg);
			break;
		default:
			break;
	}

	return 0;
}
