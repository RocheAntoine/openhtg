//
// Created by antoine on 25/05/2020.
//
#include "openHyperTreeGridCerealSerialization.h"
#include "openHyperTree.h"
#include "openHyperTreeGrid.h"

#include <fstream>
#include <sstream>
#include <iostream>

#include "cereal/types/vector.hpp"
#include "cereal/types/map.hpp"
#include "cereal/types/array.hpp"
#include "cereal/types/memory.hpp"
#include "cereal/archives/binary.hpp"

void openhtg::io::mcereal::writeInFile(const std::string &filename, openHyperTreeGrid & htg)
{
	std::ofstream file(filename, std::ofstream::binary);

	{
		cereal::BinaryOutputArchive oarchive(file);

		oarchive(htg);
	}

	file.close();
}

void openhtg::io::mcereal::loadFromFile(const std::string &filename, openHyperTreeGrid & htg)
{
	std::ifstream file(filename, std::ifstream::binary);

	if (!file.is_open())
	{
		std::cerr << "Cannot open file" << std::endl;
		exit(EXIT_FAILURE);
	}

	{
		cereal::BinaryInputArchive iarchive(file);

		iarchive(htg);
	}
	file.close();
}