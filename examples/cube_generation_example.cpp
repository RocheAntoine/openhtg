//
// Created by antoine on 26/06/2020.
//

#include <iostream>
#include "openHyperTreeGridCerealSerialization.h"
#include "openHyperTree.h"
#include "openHyperTreeGrid.h"
#include "dataGeneration.h"

int main()
{
	openHyperTreeGrid htg;

	double minBounds[3] = {0, 0, 0};
	double maxBounds[3] = {100,100,100};
	htg.setBoundingBox(minBounds, maxBounds);
	uint32_t dimensions[3] = {1,1,1};
	htg.setDimensions(dimensions);

	dataGeneration::generateDifferentSizeCubes(htg, 3);

	openhtg::io::mcereal::writeInFile("sphere_example.shtg", htg);
	openHyperTreeGrid htg2;
	openhtg::io::mcereal::loadFromFile("sphere_example.shtg", htg2);
	if (!htg.isEqualTo(htg2))
	{
		std::cerr << "Created HTG and read / written one are not the same" << std::endl;
		exit(EXIT_FAILURE);
	}
	else
	{
		std::cout << "Sphere written in disk" << std::endl;
	}
	return 0;
}
