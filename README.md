## Open HyperTreeGrid

Light version of the VTK-HyperTreeGrid data structure
Some VTK features are no implemented here :
* Octree only (3D, 2:1 rafinement ratio)
* Uniform grid only
* Sequential computation
* No file reading option (level limit, scalar fields ...)
* Float scalar only

OpenHTG Features :
* VTK-HTG <-> OpenHTG conversion
* Template cursor
* Cursor iterator
* Fast IO with Cereal
* Data generation : AMR sphere

No dependency except VTK if needed

### Installation

* git clone https://gitlab.com/RocheAntoine/openhtg
* mkdir <build_dir>
* cd <build_dir>
* cmake <source_dir>
* ccmake <build_dir>
* make (-j)
* make install (optional)
